<h1 align="center">Hi 👋, I'm Afshin</h1>
<h3 align="center">I am Afshin. Student of software engineering.<h3>
<h3 align="center">I love learning and programming and I am always looking for an opportunity to learn more and develop.
</h3>



- 🦁 Hello , welcome to my **[github](https://github.com/afsh7n "github")** page 

- 💬 Ask me if you have any questions

<h1></h1>


<p align="center">
    <span align="center">
    <a href="https://github.com/afsh7n?tab=repositories" title="Profile">
        <img src="https://github-readme-stats.vercel.app/api?username=afsh7n&show_icons=true&theme=graywhite&border_color=aaa&custom_title=My%20GitHub%20Stats" alt="Github Stats" />
    </a>
</span>
<span align="center">
    <a href="https://github.com/afsh7n?tab=repositories" title="Profile">
        <img src="https://github-readme-streak-stats.herokuapp.com/?user=afsh7n&theme=graywhite&border_color=aaa" alt="afsh7n" />
   </a>
</span>
</p>
<p align="center">
    <a href="https://github.com/afsh7n?tab=repositories" title="Profile">
        <img src="https://github-readme-stats.vercel.app/api/top-langs/?username=afsh7n&layout=compact&langs_count=10&theme=graywhite&border_color=aaa&custom_title=My%20Most%20Used%20Langauges&include_all_commits=true&count_private=true" alt="Github Stats" />
    </a>
</p>

<h2 align="center">🌟 Socials 🌟</h2>

<p align="center">
    <a href="https://g.dev/afsh7n" title="Developer Profile">
        <img src="./assets/images/Gdev.svg" alt="Gmail" width="90" /></a>
    <a href="afsh7n@gmail.com?subject=Hi%20from%20Github" title="Gmail">
        <img src="./assets/images/Gmail.svg" alt="Gmail" width="60" /></a>
    <a href="https://www.instagram.com/afsh7n_/" title="Instagram">
        <img src="./assets/images/Instagram.svg" alt="Instagram" width="50" /></a>
    <a href="https://twitter.com/afsh7n" title="Twitter">
        <img src="./assets/images/Twitter.svg" alt="Twitter" width="50" /></a>
    <a href="https://dev.to/afsh7n" title="Dev.to">
        <img src="./assets/images/Dev.svg" alt="Dev.to" width="50" /> </a>
    <a href="https://wa.link/23yh4v" title="Whatsapp">
        <img src="./assets/images/Whatsapp.svg" alt="Whatsapp" width="50"> </a>
    <a href="https://www.linkedin.com/in/afsh7n/" title="LinkedIn">
        <img src="./assets/images/Linkedin.svg" alt="LinkedIn" width="50" /></a>
</p>
<p align="center">💙 If you like my projects, Give them ⭐ and Share it with friends!</p>
</p>

<p align="center">
        <img src="./assets/images/Bottom.svg" alt="Github Stats" />
</p>
